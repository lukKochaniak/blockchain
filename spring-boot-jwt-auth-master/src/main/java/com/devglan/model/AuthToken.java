package com.devglan.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthToken {

    private String token;
    private String username;

    public AuthToken(String token, String username){
        this.token = token;
        this.username = username;
    }

}
